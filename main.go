package main

import (
	"fmt"

	"github.com/tebeka/selenium"
	"github.com/tebeka/selenium/chrome"
)

func main() {
	// Chemin vers le WebDriver
	const path = "C:\\Chromedriver\\chromedriver.exe"

	// Créer des options pour le navigateur Chrome
	opts := []selenium.ServiceOption{}
	caps := selenium.Capabilities{"browserName": "chrome"}
	chromeCaps := chrome.Capabilities{
		Args: []string{
			//"--headless", // Exécuter Chrome en mode headless (sans interface graphique)
		},
	}
	caps.AddChrome(chromeCaps)

	// Démarrer le service Selenium
	_, err := selenium.NewChromeDriverService(path, 4444, opts...)
	if err != nil {
		fmt.Println("Erreur lors du démarrage du service ChromeDriver :", err)
		return
	}

	// Créer une instance du navigateur Chrome
	wd, err := selenium.NewRemote(caps, fmt.Sprintf("http://localhost:%d/wd/hub", 4444))
	if err != nil {
		fmt.Println("Erreur lors de la création de l'instance du navigateur :", err)
		return
	}

	// Charger une page Web
	if err := wd.Get("https://skin.club/fr/cases/free-case"); err != nil {
		fmt.Println("Erreur lors du chargement de la page :", err)
		return
	}

	// Effectuer des actions automatisées (par exemple, cliquer sur un élément)
	productElement, err := wd.FindElement(selenium.ByCSSSelector, ".take-part-button")
	if err != nil {
		fmt.Println("Error:", err)
	}
	productElement.Click()
	fmt.Println("Script terminé avec succès!")
}
